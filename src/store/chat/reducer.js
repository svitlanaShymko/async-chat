import { createReducer } from '@reduxjs/toolkit';
import { DataStatus, ActionStatus } from 'common/enums/enums';
import { fetchMessages, addMessage, updateMessage, deleteMessage } from './actions';

const initialState = {
  messages: [],
  status: DataStatus.IDLE,
  isSaving: false,
};

const reducer = createReducer(initialState, (builder) => {
  builder.addCase(fetchMessages.pending, (state) => {
    state.status = DataStatus.PENDING;
  });

  builder.addCase(fetchMessages.fulfilled, (state, { payload }) => {
    const { messages} = payload;

    state.messages = messages;
    state.status = DataStatus.SUCCESS;
  });

  builder.addCase(addMessage.fulfilled, (state, { payload }) => {
    const { message } = payload;
    state.messages = state.messages.concat(message);
  });

  builder.addCase(updateMessage.fulfilled, (state, { payload }) => {
    const { message } = payload;
    state.messages = state.messages.map((it) => {
      return it.id === message.id ? { ...it, ...message } : it;
    });
  });

  builder.addCase(deleteMessage.fulfilled, (state, { payload }) => {
    const { messageId } = payload;
    state.messages=state.messages.filter((it) => it.id !== messageId);
  });

  builder.addMatcher((action) => action.type.endsWith(ActionStatus.REJECTED), (state) => {
    state.status = DataStatus.ERROR;
  });
});

export { reducer };
