import { createAsyncThunk } from '@reduxjs/toolkit';
import { ActionType } from './common';

const fetchMessages = createAsyncThunk(ActionType.FETCH_MESSAGES, async (_args, { extra }) => ({
  messages: await extra.messagesService.getAll(),
}));

const addMessage = createAsyncThunk(ActionType.ADD_MESSAGE, async (payload, { extra }) => ({
  message: await extra.messagesService.create(payload),
}))

const updateMessage = createAsyncThunk(ActionType.UPDATE_MESSAGE, async (payload, { extra }) => ({
  message: await extra.messagesService.update(payload),
}));

const deleteMessage = createAsyncThunk(ActionType.DELETE_MESSAGE, async (messageId, { extra }) => {
  await extra.messagesService.delete(messageId);

  return {
    messageId
  };
});


export {
  fetchMessages,
  addMessage,
  updateMessage,
  deleteMessage,
};
