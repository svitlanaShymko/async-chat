import { createReducer } from '@reduxjs/toolkit';
import { DataStatus, ActionStatus } from 'common/enums/enums';
import { setUser, removeUser } from './actions';

const initialState = {
  user: null,
  status: DataStatus.IDLE,
};

const reducer = createReducer(initialState, (builder) => {
  builder.addCase(setUser.pending, (state) => {
    state.status = DataStatus.PENDING;
  });
  builder.addCase(setUser.fulfilled, (state, { payload }) => {
    const { user } = payload;
    state.user = user;
    state.status = DataStatus.SUCCESS;
  });

  builder.addCase(removeUser, (state) => {
    Object.assign(state, initialState);
  })

  builder.addMatcher((action) => action.type.endsWith(ActionStatus.REJECTED), (state) => {
    state.status = DataStatus.ERROR;
  });
});

export { reducer };
