import { createAction, createAsyncThunk } from '@reduxjs/toolkit';
import { ActionType } from './common';


const setUser= createAsyncThunk(ActionType.SET_USER, async (payload, { extra }) => ({
  user: await extra.usersService.search(payload),
}));

const removeUser = createAction(ActionType.REMOVE_USER, () => ({

}));

export {
  setUser,
  removeUser
};
