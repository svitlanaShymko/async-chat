import { createAsyncThunk } from '@reduxjs/toolkit';
import { ActionType } from './common';


const fetchUsers = createAsyncThunk(ActionType.FETCH_USERS, async (_args, { extra }) => ({
  users: await extra.usersService.getAll(),
}));

const addUser = createAsyncThunk(ActionType.ADD_USER, async (payload, { extra }) => ({
  user: await extra.usersService.create(payload),
}))

const updateUser = createAsyncThunk(ActionType.UPDATE_USER, async (payload, { extra }) => ({
  user: await extra.usersService.update(payload),
}));

const deleteUser = createAsyncThunk(ActionType.DELETE_USER, async (userId, { extra }) => {
  await extra.usersService.delete(userId);

  return {
    userId,
  };
});

export {
  fetchUsers,
  addUser,
  updateUser,
  deleteUser,
};
