import { configureStore } from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga';
import { users as usersService } from 'services/services';
import { messages as messagesService } from 'services/services';
import {  users, chat, auth} from './root-reducer';

const sagaMiddleware = createSagaMiddleware();

const store = configureStore({
  reducer: {
    users,
    chat,
    auth
  },
  middleware: (getDefaultMiddleware) => {
    return getDefaultMiddleware({
      thunk: {
        extraArgument: {
          usersService,
          messagesService,
        },
      },
      serializableCheck: false,
    }).concat(sagaMiddleware);
  },
});

export { store };
