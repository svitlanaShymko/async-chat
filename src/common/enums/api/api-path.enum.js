const ApiPath = {
  USERS: '/users',
  MESSAGES: '/messages',
};

export { ApiPath };
