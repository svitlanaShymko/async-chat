const AppPath = {
  LOGIN: '/login',
  USERS: '/users',
  USERS_$ID: '/users/:id',
  CHAT: '/',
  ROOT: '/',
  ANY: '*',
};

export { AppPath };
