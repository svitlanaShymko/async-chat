const DataPlaceholder = {
  PAGE_NOT_FOUND: 'Oops... page not found',
};

export { DataPlaceholder };
