import React, {useMemo} from 'react';
import Preloader from '../Preloader/Preloader';
import ChatHeader from '../ChatHeader/ChatHeader';
import MessageList from '../MessageList/MessageList';
import { chat as chatActionCreator } from 'store/actions';
import MessageInput from '../MessageInput/MessageInput';
import { useCallback, useEffect, useState, useDispatch, useSelector } from 'hooks/hooks';
import './Chat.css';
import EditMessageModal from '../../components/EditMessageModal/EditMessageModal';


const Chat = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [messageToEdit, setMessageToEdit] = useState('');

  const { user } = useSelector(({ auth }) => ({
    user: auth.user,
  }))
  

  const isLoading = false;

  const { messages } = useSelector(({ chat }) => ({
    messages: chat.messages,
  }));

const dispatch = useDispatch();

const handleMessageAdd = useCallback((text) => {
  const newMessage = {
        user: user.name,
        userId: user.id,
        text,
        createdAt: new Date().toISOString(),
        editedAt: '',
        avatar: ''
      };
  dispatch(chatActionCreator.addMessage(newMessage));
}, [dispatch, user.name, user.id]);


const handleMessageUpdate = useCallback((text) => {
  dispatch(chatActionCreator.updateMessage({...messageToEdit, text}));
}, [dispatch, messageToEdit]);

  const handleMessageDelete = useCallback((messageId) => {
    dispatch(chatActionCreator.deleteMessage(messageId));
  }, [dispatch]);

  useEffect(() => {
    dispatch(chatActionCreator.fetchMessages());
  }, [dispatch]);

  const openMessageModal = useCallback(
    (message) => {
      setIsModalOpen(true);
      setMessageToEdit(message);
    },
    [setIsModalOpen, setMessageToEdit]
  )

  const closeMessageModal = ()=>{
    setIsModalOpen(false);
  }

  const countUsers = (messages) => {
    return new Set(messages.map((message) => message.user)).size; 
  };

  const getLatestMessageDate = useMemo(() => {
    const dateArr = messages.map(message=>message.createdAt).sort((a, b) => new Date(b).getTime() - new Date(a).getTime());
    return dateArr[0];
  }, [messages]);


  return isLoading ? (
    <Preloader />
  ) : (
    <div className="chat">
      <ChatHeader
        usersCount={countUsers(messages)}
        messageCount={messages.length}
        lastMessageDate={getLatestMessageDate}
      />
      <MessageList
        messages={messages}
        deleteMessage={handleMessageDelete}
        openMessageModal={openMessageModal}
        toggleLikeMessage={()=>{}}
      />
      <MessageInput sendMessage={handleMessageAdd} />
      {isModalOpen &&
      <EditMessageModal initialText={messages.find(message=>message.id===messageToEdit.id)?.text ?? ''} closeMessageModal={closeMessageModal} editMessage={handleMessageUpdate}/>
} 
    </div>
  );
};

export default Chat;
