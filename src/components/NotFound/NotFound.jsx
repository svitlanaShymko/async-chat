import './style.css';

const NotFound = () => (
  <section className="page-notfound">
    <h2>Page not found</h2>
  </section>
);

export default NotFound;
