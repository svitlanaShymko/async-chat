
import AddUserModal from 'components/AddUserModal/AddUserModal';
import { users as usersActionCreator } from 'store/actions';
import { useCallback, useEffect, useState, useDispatch, useSelector } from 'hooks/hooks';
import User from 'components/User/User';
import React from 'react';
import './Users.css';


const Users = ()=>{
    const [isModalOpen, setIsModalOpen] = useState(false);

    const toggleModalOpen = ()=>{
        setIsModalOpen(!isModalOpen);
    }

    const { users } = useSelector(({ users }) => ({
        users: users.users,
      }));
    
    const dispatch = useDispatch();

    const handleUserAdd = useCallback((user) => {
      dispatch(usersActionCreator.addUser({...user, isAdmin: false, password: '124'}));
    }, [dispatch]);


    const handleUserUpdate = useCallback((user) => {
      dispatch(usersActionCreator.updateUser(user));
    }, [dispatch]);
    
      const handleUserDelete = useCallback((userId) => {
        dispatch(usersActionCreator.deleteUser(userId));
      }, [dispatch]);
    
      useEffect(() => {
        dispatch(usersActionCreator.fetchUsers());
      }, [dispatch]);

    return(
        <div className="users-container">
        <button onClick={toggleModalOpen}>Add User</button>
        <div >
        {users.map(user=>(
            <User key={user.id} user={user} onSaveUser={handleUserUpdate} onDeleteUser={handleUserDelete} isEditable={!user.isAdmin}/>
        ))}
        </div>
        {isModalOpen && <AddUserModal addUser={handleUserAdd} closeModal={toggleModalOpen}/>}
        </div>

    );
}

export default Users;