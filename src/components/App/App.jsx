import { AppPath } from 'common/enums/enums';
import { Switch, Route, Redirect } from 'react-router-dom';
import Users from 'components/Users/Users';
import Chat from 'components/Chat/Chat';
import Header from 'components/Header/Header';
import NotFound from 'components/NotFound/NotFound';
import LoginPage from 'components/LoginPage/LoginPage';
import { useCallback, useDispatch, useSelector } from 'hooks/hooks';
import { auth as authActionCreator } from 'store/actions';

const App = () => {
  const { user } = useSelector(({ auth }) => ({
    user: auth.user
  }));

  const dispatch = useDispatch();

const onLogOut = useCallback(()=>{
  dispatch(authActionCreator.removeUser());
}, [dispatch])

  return (
  <>
    {user && <Header user={user} onLogOut={onLogOut}/>}
    <main>
      <Switch>
      <Route exact path={AppPath.ROOT}>
        {!user ? <Redirect to={AppPath.LOGIN}/> : <Chat />}
      </Route>
        <Route path={AppPath.LOGIN}>
        {user ? <Redirect to={AppPath.ROOT}/>: <LoginPage />}
        </Route>
        {user && <Route exact path={AppPath.ROOT} component={Chat}/>}
        {user && user?.isAdmin && <Route path={AppPath.USERS} exact component={Users} />}
        <Route path={AppPath.ANY} exact component={NotFound}  />
      </Switch>
    </main>
  </>
);
}

export default App;
