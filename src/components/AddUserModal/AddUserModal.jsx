// import { faPaperPlane } from '@fortawesome/free-solid-svg-icons';
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useState } from 'react';
import './AddUserModal.css';

const AddUserModal= ({ closeModal, addUser }) => {
  const [userName, setUserName] = useState('');
  const [userEmail, setUserEmail] = useState('');

  const handleNameChange = (event)=>{
      setUserName(event.target.value)
  }

  const handleEmailChange = (event)=>{
      setUserEmail(event.target.value)
  }
      

  return (
    <div className="edit-message-modal">
      <div className="modal-shown">
        <div className="edit-modal-title">Add User</div>
        <span className="edit-message-close" onClick={closeModal}>&times;</span>
        <div className="input-wrapper">
        <input defaultValue={userName} onChange={handleNameChange}/>
        <input defaultValue={userEmail} onChange={handleEmailChange}/>
          <button className="edit-message-button" onClick={() => {
                  addUser({name:userName, email: userEmail });
                  closeModal();
                }}>
            Add
          </button>
      </div>
      </div>
    </div>
  );
};

export default AddUserModal;
