
import React, { useState } from 'react';
import './User.css';



const User = ({user, onSaveUser, onDeleteUser, isEditable = true})=>{
    const {id, name, email} = user;
    const [isEditing, setIsEditing] = useState(false);
    const [userName, setUserName] = useState(name);
    const [userEmail, setUserEmail] = useState(email);
    
    const toggleIsEditing = ()=>{
        setIsEditing(!isEditing);
    }

    const handleNameChange = (event)=>{
        setUserName(event.target.value)
    }

    const handleEmailChange = (event)=>{
        setUserEmail(event.target.value)
    }

    const handleSaveUser= ()=>{
        onSaveUser({...user, name: userName, email: userEmail});
        setIsEditing(!isEditing);
    }

    return(
            <div key={id} className="user-item">
                {isEditing ?
                <>
                <input defaultValue={userName} onChange={handleNameChange} className="user-info"/>
                <input defaultValue={userEmail} onChange={handleEmailChange} className="user-info"/>
                </>
                :
                <><div className="user-info">{userName}</div><div className="user-info">{userEmail}</div></>
                }

                <div className="buttons-container">
                    {!isEditing
                    ? <button onClick={toggleIsEditing} disabled={!isEditable}>Edit</button> 
                    :<button onClick={handleSaveUser} disabled={!isEditable}>Save</button> }
                    <button onClick={()=>onDeleteUser(id)} disabled={!isEditable}>Delete</button>
                </div>
            </div>

    );
}

export default User;