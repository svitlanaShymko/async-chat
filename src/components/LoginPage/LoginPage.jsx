import { useDispatch, useCallback } from 'hooks/hooks';
import { auth as authActionCreator } from 'store/actions';
import React, { useState } from 'react';
import './LoginPage.css';


const LoginPage= () => {
  const [userName, setUserName] = useState('');
  const [userPassword, setUserPassword] = useState('');

  const dispatch = useDispatch();

  const handleNameChange = (event) =>{
      setUserName(event.target.value)
  }

  const handlePasswordChange = (event )=>{
      setUserPassword(event.target.value)
  }

  const handleLogin = useCallback(() => {
    dispatch(authActionCreator.setUser({name: userName, password: userPassword}));
  }, [dispatch, userPassword, userName]);

  return (
    <div className="login-container">
      <div>Name</div>
        <input defaultValue={userName} onChange={handleNameChange} placeholder="name"/>
        <div>Password</div>
        <input defaultValue={userPassword} onChange={handlePasswordChange} type="password"/>
          <button className="" onClick={handleLogin}>Login
          </button>
          <div>(Try admin/admin, or niko/1234)</div>
    </div>
  );
};

export default LoginPage;
