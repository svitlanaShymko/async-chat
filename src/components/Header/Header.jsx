import { AppPath } from 'common/enums/enums';
import { Link } from 'react-router-dom';
import './Header.css';

const Header = ({user, onLogOut}) => (
  <header className="page-header">
    <nav>
      <Link to={AppPath.ROOT} className="nav-item">Chat</Link>
      {user?.isAdmin && <Link to={AppPath.USERS} className="nav-item">Users</Link>}
    </nav>
    <button onClick={onLogOut}>LOG OUT</button>
  </header>
);

export default Header;
