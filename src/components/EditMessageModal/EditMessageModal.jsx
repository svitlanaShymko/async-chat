import { faPaperPlane } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useState } from 'react';
import './EditMessageModal.css';



const EditMessageModal = ({ initialText, closeMessageModal, editMessage }) => {
  const [inputText, setInputText] = useState(initialText);

  const handleInputChange = (event) => {
    setInputText(event.target.value);
  };

  return (
    <div className="edit-message-modal">
      <div className="modal-shown">
        <div className="edit-modal-title">Edit Message</div>
        <span className="edit-message-close" onClick={closeMessageModal}>&times;</span>
        <div className="input-wrapper">
          <textarea className="edit-message-input" onChange={handleInputChange} defaultValue={initialText} />
          <button className="edit-message-button" onClick={() => {
                  editMessage(inputText);
                  closeMessageModal();
                }}>
            <FontAwesomeIcon icon={faPaperPlane} size="lg"/>
          </button>
      </div>
      </div>
    </div>
  );
};

export default EditMessageModal;
