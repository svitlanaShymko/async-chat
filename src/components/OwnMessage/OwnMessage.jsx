import { faPen, faTrash} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import { formatTime } from '../../helpers/helpers';
import './OwnMessage.css';

const OwnMessage = ({ message, deleteMessage, openMessageModal }) => {;
  const { id, text, createdAt, user } = message;
  return (
    <div className="own-message">
      <div className="message-header">
        <div className="message-user-name">{user}</div>
        <div className="message-time">{formatTime(createdAt)}</div>
      </div>
        <div className="message-text">{text}</div>
      <div className="icons-container">
        <FontAwesomeIcon
          icon={faPen}
          className="icon message-edit"
          onClick={()=>openMessageModal(message)}
        />
        <FontAwesomeIcon
          icon={faTrash}
          className="icon message-delete"
          onClick={() => deleteMessage(id)}
        />
      </div>
    </div>
  );
};

export default OwnMessage;
