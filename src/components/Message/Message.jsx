import React from 'react';
import { faHeart } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { formatTime } from '../../helpers/helpers';
import './Message.css';

const Message = ({ message, toggleLikeMessage }) => {
  const { id, text, createdAt, user, avatar, isLiked } = message;
  return (
    <div className="message">
      <img src={avatar} alt="user-avatar" className="message-user-avatar" />
      <div className="message-content">
        <div className="message-header">
          <div className="message-user-name">{user}</div>
          <div className="message-time">{formatTime(createdAt)}</div>
        </div>
        <div className="message-text">{text}</div>
        <div className={isLiked ? "message-liked" : "message-like"} onClick={()=>toggleLikeMessage(id)}>
        <FontAwesomeIcon icon={faHeart} /></div>
      </div>
    </div>
  );
};

export default Message;
