import React from 'react';
import { groupMessagesByDate } from '../../helpers/helpers';
import Message from '../Message/Message';
import OwnMessage from '../OwnMessage/OwnMessage';
import './MessageList.css';
import { useSelector } from 'react-redux';

const MessageList = ({ messages, deleteMessage, openMessageModal, toggleLikeMessage }) => {
  const { user } = useSelector(({ auth }) => ({
    user: auth.user,
  }))
  
  return (
    <div className="message-list">
      {groupMessagesByDate(messages).map((messagesByDate, id) => (
        <div className="date-container" key={id}>
          <div className="messages-divider">{messagesByDate.date}</div>
          {messagesByDate.messages.map((message, index) =>
            message.userId === user.id ? (
              <OwnMessage
                key={index}
                message={message}
                deleteMessage={deleteMessage}
                openMessageModal={openMessageModal}
              />
            ) : (
              <Message key={index} message={message} toggleLikeMessage={toggleLikeMessage} />
            ),
          )}
        </div>
      ))}
    </div>
  );
};

export default MessageList;
