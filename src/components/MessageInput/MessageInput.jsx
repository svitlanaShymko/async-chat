import { faPaperPlane } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useState } from 'react';
import './MessageInput.css';


const MessageInput = ({ sendMessage }) => {
  const [inputText, setInputText] = useState('');

  const handleInputChange = (event) => {
    setInputText(event.target.value);
  };

  const handleSendMessage = () =>{
    sendMessage(inputText);
    setInputText('');
  }

  return (
    <div className="message-input">
      <textarea
        value={inputText}
        className="message-input-text"
        placeholder="Type your message..."
        onChange={handleInputChange}
      />
      <button className="message-input-button" onClick={() => handleSendMessage()}>
      <FontAwesomeIcon icon={faPaperPlane} size="lg"/>
      </button>
    </div>
  );
};

export default MessageInput;
