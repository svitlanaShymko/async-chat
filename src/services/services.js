import { ENV } from 'common/enums/enums';
import { Http } from './http/http.service';
import { Users } from './users/users.service';
import { Messages } from './messages/messages.service';

const http = new Http();

const users = new Users({
  baseUrl: ENV.API.URL,
  http,
});

const messages = new Messages({
  baseUrl: ENV.API.URL,
  http,
});

export { http, users, messages  };
