import { ApiPath, ContentType, HttpMethod } from 'common/enums/enums';

class Users {
  constructor({ baseUrl, http }) {
    this._baseUrl = baseUrl;
    this._http = http;
    this._basePath = ApiPath.USERS;
  }
  
  async search(userData){
    const { name, password } = userData;
    const users = await this.getAll();
    const user = users.find(dbUser=> dbUser.name === name && dbUser.password === password);
    if(!user) {
      return null;
    }
    return user;
}

  getAll() {
    return this._http.load(this._getUrl(), {
      method: HttpMethod.GET,
    });
  }

  getOne(id) {
    return this._http.load(this._getUrl(id), {
      method: HttpMethod.GET,
    });
  }

  create(payload) {
    return this._http.load(this._getUrl(), {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload),
    });
  }

  update(payload) {
    return this._http.load(this._getUrl(payload.id), {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload),
    });
  }

  partialUpdate(id, payload) {
    return this._http.load(this._getUrl(id), {
      method: HttpMethod.PATCH,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload),
    });
  }

  delete(id) {
    return this._http.load(this._getUrl(id), {
      method: HttpMethod.DELETE,
    });
  }

  _getUrl(path = '') {
    return `${this._baseUrl}${this._basePath}/${path}`;
  }
}

export { Users };