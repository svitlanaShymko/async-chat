import { ApiPath, ContentType, HttpMethod } from 'common/enums/enums';


class User {
  constructor({ baseUrl, http }) {
    this._baseUrl = baseUrl;
    this._http = http;
    this._basePath = ApiPath.USERS;
  }


  search(userData){
    console.log("USERDATA:", userData)
    const { name, password } = userData;
    debugger;
    const users = this.getAll();
    const user = users.find(dbUser=> dbUser.name === name && dbUser.password === password);
    console.log("USER:", user)
    if(!user) {
      console.log("User doesnt exist");
      return null;
    }
    return user;
}

  getAll() {
    return this._http.load(this._getUrl(), {
      method: HttpMethod.GET,
    });
  }

}

export { User };